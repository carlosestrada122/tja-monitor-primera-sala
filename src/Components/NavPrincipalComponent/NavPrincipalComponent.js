import React from "react";
import styles from "./NavPrincipalComponent.module.css";
import imagenTribunal from "../../Images/tjaicon.png";
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import Popup from "reactjs-popup";
import "reactjs-popup/dist/index.css";
import imagenIntranet from "../../Images/intranetLogo.png";
import logoTJA from "../../Images/tjaicon.png";
import { AuthContext } from "../../auth/authContext";
import jwt_decode from "jwt-decode";
import { types } from "./../../types/types";

const Capitalize = function (string) {
  const lower = string.toLowerCase();

  return lower.replace(/(?:^|\s)\S/g, function (a) {
    return a.toUpperCase();
  });
};

export const NavPrincipalComponent = () => {
  const { user, dispatch } = React.useContext(AuthContext);

  console.log(user);

  var decoded = jwt_decode(user.token);
  return (
    <div className={`${styles.fondoMenu}`}>
      <div className="container">
        <div className="row d-flex align-items-center">
          <div
            className="col-sm-6"
            style={{ cursor: "pointer" }}
            onClick={() => {
              window.location.href = "/";
            }}
          >
            <img src={imagenTribunal} alt="Prueba" height={25} />{" "}
            <strong className="ms-2 d-none d-sm-inline">
              Monitor de avances expedientes
            </strong>
          </div>
          <div className="col-sm-6 text-end">
            <Popup
              trigger={
                <span
                  className={`${styles.disableSelect}`}
                  style={{ cursor: "pointer" }}
                >
                  <small className="fw-normal">
                    {Capitalize(decoded.first_name)}{" "}
                    {Capitalize(decoded.last_name)}
                  </small>
                  <KeyboardArrowDownIcon fontSize="small" />
                  <br></br>
                  <small className={`fw-light me-4 ${styles.puesto}`}>
                    {Capitalize(decoded.title)}
                  </small>
                </span>
              }
              position="bottom right"
            >
              <ul
                class="list-group list-group-flush"
                style={{ fontSize: "13px" }}
              >
                <li
                  class="list-group-item list-group-item-action"
                  onClick={() => {
                    window.open("https://intranet.tjagto.gob.mx/", "_blank");
                  }}
                >
                  <img
                    alt="Abrir Intranet"
                    src={imagenIntranet}
                    height={15}
                    className="pe-2"
                  />
                  Ir a Intranet
                </li>
                <li
                  class="list-group-item list-group-item-action"
                  onClick={() => {
                    window.open(
                      "https://serviciosdigitales.tjagto.gob.mx/tribunalelectronicoweb/Dashboard.aspx",
                      "_blank"
                    );
                  }}
                >
                  <img
                    alt="Abrir Intranet"
                    src={logoTJA}
                    height={15}
                    className="pe-2"
                  />
                  Sistema Expedientes
                </li>
                <li
                  class="list-group-item list-group-item-action ps-4"
                  onClick={() => {
                    dispatch({
                      type: types.logout,
                    });
                    window.location.href = "/";
                  }}
                >
                  Cerrar Sesión
                </li>
              </ul>
            </Popup>
          </div>
        </div>
      </div>
    </div>
  );
};
