import React from "react";
import styles from "./HallSelectorComponent.module.css";
import Popup from "reactjs-popup";
import "reactjs-popup/dist/index.css";
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import MoreVertIcon from "@mui/icons-material/MoreVert";
export const HallSelectorComponent = () => {
  return (
    <>
      <div className={`${styles.fondoMenu}`}>
        <div className="container">
          <div className="row">
            <div className="col-12 d-flex justify-content-between">
              <div>
                <Popup
                  trigger={
                    <span
                      className={`${styles.disableSelect}`}
                      style={{ cursor: "pointer" }}
                    >
                      <span className={`${styles.hallNumber}`}>1</span>{" "}
                      <span className={`${styles.hallString}`}>
                        Primera Sala
                      </span>
                      <KeyboardArrowDownIcon fontSize="small" />
                      <br></br>
                    </span>
                  }
                  position="bottom left"
                >
                  <ul
                    class="list-group list-group-flush"
                    style={{ fontSize: "13px" }}
                  >
                    <li
                      class="list-group-item list-group-item-action"
                      onClick={() => {
                        window.open(
                          "https://intranet.tjagto.gob.mx/",
                          "_blank"
                        );
                      }}
                    >
                      Primera Sala
                    </li>
                  </ul>
                </Popup>
              </div>
              <div>
                <Popup
                  trigger={
                    <span
                      className={`${styles.disableSelect}`}
                      style={{ cursor: "pointer" }}
                    >
                      <MoreVertIcon fontSize="small" />
                      <br></br>
                    </span>
                  }
                  position="bottom right"
                >
                  <ul
                    class="list-group list-group-flush"
                    style={{ fontSize: "13px" }}
                  >
                    <li
                      class="list-group-item list-group-item-action"
                      onClick={() => {
                        window.open(
                          "https://intranet.tjagto.gob.mx/",
                          "_blank"
                        );
                      }}
                    >
                      Mostrar Todos
                    </li>
                    <li
                      class="list-group-item list-group-item-action"
                      onClick={() => {
                        window.open(
                          "https://intranet.tjagto.gob.mx/",
                          "_blank"
                        );
                      }}
                    >
                      Mostrar mi trabajo
                    </li>
                  </ul>
                </Popup>
              </div>
            </div>
            <div className="col-12">
              <hr style={{ marginTop: "20px", opacity: "0.1" }} />
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
