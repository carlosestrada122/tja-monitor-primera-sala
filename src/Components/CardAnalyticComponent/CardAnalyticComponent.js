import React from "react";
import styles from "./CardAnalyticComponent.module.css";
import { toast } from "react-toastify";
export const CardAnalyticComponent = (props) => {
  return (
    <div
      style={{ background: props.colorCard, color: props.colorTexto }}
      className={`card ${styles.card}`}
      onClick={() => {
        toast.info("Haz seleccionado " + props.titulo);
      }}
    >
      <div className="card-body">
        <span className="fw-light">{props.titulo}</span>
        <p className="m-0 text-end fw-bold h4">00</p>
      </div>
    </div>
  );
};
