import React from "react";
import { ToastContainer, toast } from "react-toastify";
import { AppRouter } from "./Routers/AppRouter.js";
import { AuthContext } from "./auth/authContext";
import { authReducer } from "./auth/authReducer";
import { io } from "socket.io-client";
import "animate.css";
import { Helmet, HelmetProvider } from "react-helmet-async";
import ReactDOM from "react-dom";
import "./Styles/fonts.css";
import "react-toastify/dist/ReactToastify.css";
const init = () => {
  return JSON.parse(localStorage.getItem("user")) || { logged: false };
};

function App() {
  const [user, dispatch] = React.useReducer(authReducer, {}, init);

  React.useEffect(() => {
    localStorage.setItem("user", JSON.stringify(user));
  }, [user]);

  return (
    <HelmetProvider>
      <AuthContext.Provider value={{ user, dispatch }}>
        <ToastContainer />
        <AppRouter />
      </AuthContext.Provider>
    </HelmetProvider>
  );
}
ReactDOM.hydrate(App, document.getElementById("root"));

export default App;
