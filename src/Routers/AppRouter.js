import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Routes,
  Route,
} from "react-router-dom";
import { AuthContext } from "../auth/authContext";
import { HallSelectorComponent } from "../Components/HallSelectorComponent/HallSelectorComponent";
import { NavPrincipalComponent } from "../Components/NavPrincipalComponent/NavPrincipalComponent";
import { SetWindowNameHelperComponent } from "../Helpers/SetWindowNameHelperComponent";
import { LoginScreenComponent } from "../Screens/LoginScreenComponent/LoginScreenComponent";
import { MainScreenComponent } from "../Screens/MainScreenComponent/MainScreenComponent";
import { PrivateRoute } from "./PrivateRoute";
import { PublicRoute } from "./PublicRoute";

export const AppRouter = () => {
  const { user, dispatch } = React.useContext(AuthContext);

  return (
    <Router>
      <Routes>
        <Route
          path="/login"
          element={
            <PublicRoute>
              <SetWindowNameHelperComponent windowName={"Iniciar Sesión"} />
              <LoginScreenComponent />
            </PublicRoute>
          }
        ></Route>

        <Route
          path="/*"
          element={
            <PrivateRoute>
              <NavPrincipalComponent />
              <HallSelectorComponent />
              <MainScreenComponent />
            </PrivateRoute>
          }
        ></Route>
      </Routes>
    </Router>
  );
};
