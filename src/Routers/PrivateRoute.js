import React from "react";
import PropTypes from "prop-types";
import { Navigate, Redirect, Route } from "react-router-dom";
import { AuthContext } from "../auth/authContext";

export const PrivateRoute = ({ children }) => {
  const { user } = React.useContext(AuthContext);
  return user.logged ? children : <Navigate to="/login"></Navigate>;
};
