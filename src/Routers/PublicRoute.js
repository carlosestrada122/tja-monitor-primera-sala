import React from "react";
import PropTypes from "prop-types";
import { Navigate, Redirect, Route } from "react-router-dom";
import { AuthContext } from "../auth/authContext";

export const PublicRoute = ({ children }) => {
  const { user } = React.useContext(AuthContext);
  return user.logged ? <Navigate to="/login"></Navigate> : children;
};
