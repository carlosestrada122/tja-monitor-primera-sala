import React from "react";
import TextField from "@mui/material/TextField";
import { useFormik } from "formik";
import * as yup from "yup";
import Button from "@mui/material/Button";
import styles from "./LoginScreenComponent.module.css";
import { loginIntranetUser } from "../../API/ApiLoginUsers";

import jwt_decode from "jwt-decode";
import { AuthContext } from "../../auth/authContext";
import { types } from "./../../types/types";
const secretKey = "tribunal_de_justicia_administrativa_gto";

const validationSchema = yup.object({
  email: yup
    .string("Ingresa correo electrónico")
    .email("ingresa un correo válido")
    .required("Correo requerido"),
  pass: yup.string("Ingresa contraseña").required("Ingresa contraseña"),
});

export const LoginScreenComponent = () => {
  const { dispatch } = React.useContext(AuthContext);

  const formik = useFormik({
    initialValues: {
      email: "cestradagar@guanajuato.gob.mx",
      pass: "^lbkt(t88R!5%(32ux6AZq02",
    },
    validationSchema: validationSchema,
    onSubmit: (values) => {
      loginIntranetUser(values).then((resultado) => {
        var decoded = jwt_decode(resultado.data.token);
        dispatch({
          type: types.login,
          payload: resultado.data,
        });

        window.location.href = "/";
      });
    },
  });

  return (
    <>
      <div className={styles.fondoLogin}></div>
      <div className="container animate__animated animate__fadeIn">
        <div className="row">
          <div className="col-12 col-sm-10 col-lg-5 mx-auto">
            <div className="card cardLogin" style={{ marginTop: "15vh" }}>
              <div className="card-body text-center">
                <img
                  src="https://www.tjagto.gob.mx/wp-content/uploads/2021/07/Grupo-7@3x.png"
                  alt="Logotipo Inicio"
                  style={{ width: "200px", marginTop: "3vh" }}
                />
                <div className="my-4">
                  <h5>Acceder</h5>
                  <h6 className="fw-normal">
                    Sistema de monitor de expedientes
                  </h6>
                </div>
                <form onSubmit={formik.handleSubmit} className="px-5">
                  <TextField
                    margin="normal"
                    size="small"
                    fullWidth
                    id="email"
                    name="email"
                    label="Correo Acceso Intranet"
                    value={formik.values.email}
                    onChange={formik.handleChange}
                    error={formik.touched.email && Boolean(formik.errors.email)}
                    helperText={formik.touched.email && formik.errors.email}
                  />

                  <TextField
                    margin="normal"
                    size="small"
                    fullWidth
                    id="pass"
                    name="pass"
                    type="password"
                    label="Contraseña"
                    value={formik.values.pass}
                    onChange={formik.handleChange}
                    error={formik.touched.pass && Boolean(formik.errors.pass)}
                    helperText={formik.touched.pass && formik.errors.pass}
                  />

                  <div className="small py-3" style={{ textAlign: "justify" }}>
                    <small className="text-muted">
                      Para poder acceder a este sistema del Tribunal es
                      necesario el acceso con el que accedes a la Intranet,
                      recuerda proteger en todo momento tu usuario y contraseña.
                    </small>
                  </div>

                  <button
                    className="my-4 btn btn-primary w-100 text-white"
                    type="submit"
                  >
                    Iniciar Sesión
                  </button>
                </form>
              </div>
            </div>
            <div className="d-flex justify-content-between px-1 pt-2 text-muted small">
              <small>CDI TJA 2022</small>
              <small>Acerca de...</small>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
