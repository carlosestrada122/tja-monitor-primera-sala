import React from "react";
import { AuthContext } from "../../auth/authContext";
import { CardAnalyticComponent } from "../../Components/CardAnalyticComponent/CardAnalyticComponent";
import { getSeeds } from './../../API/APISeeds';

export const MainScreenComponent = () => {

  const { user, dispatch } = React.useContext(AuthContext);



  debugger;

  React.useEffect(() => {
    getSeeds(user.token).then((resultado) => {

      debugger;
    })
  }, [])
  

  return (
    <>
      {/*Esto es el componente de las promociones*/}
      <div className="container">
        <div className="row row-cols-sm-3 row-cols-md-5">
          <div className="col">
            <CardAnalyticComponent
              titulo="Sin Acordar"
              colorCard="rgba(15,32,138,0.05)"
              colorTexto="#0F208A"
            />
          </div>
          <div className="col">
            <CardAnalyticComponent
              titulo="En Revisión"
              colorCard="rgba(139,194,30,0.05)"
              colorTexto="rgba(139,194,30,1)"
            />
          </div>
          <div className="col">
            <CardAnalyticComponent
              titulo="Acordado"
              colorCard="rgba(242,129,23,0.05)"
              colorTexto="rgba(242,129,23,1)"
            />
          </div>
          <div className="col">
            <CardAnalyticComponent
              titulo="Pendiente"
              colorCard="rgba(239,12,12,0.05)"
              colorTexto="rgba(239,12,12,1)"
            />
          </div>
          <div className="col">
            <CardAnalyticComponent
              titulo="Todos"
              colorCard="rgba(0,0,0,0.05)"
              colorTexto="rgba(0,0,0,1)"
            />
          </div>
        </div>
      </div>

      <div className="container mt-4">
        <div className="row">
          <div className="col-12">
            <div
              className="p-4"
              style={{ background: "rgba(0,0,0,0.05)", borderRadius: "5px" }}
            >
              <h3 className="m-0">Valor Seleccionado</h3>
              <p className="m-0 text-muted">
                Tribunal de Justicia Administrativa del Estado de Guanajuato
              </p>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
