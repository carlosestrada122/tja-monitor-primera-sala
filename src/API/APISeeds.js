//

import { APILogin } from "../Constants/ApiConnection";
import axios from "axios";
import { toast } from "react-toastify";

export const getSeeds = async (token) => {
  let url =
    "https://intranetbeta.tjagto.gob.mx/api/v1/monexp/records/2022-02-01/2022-02-30/1";
  try {
    const response = await axios.get(url, {
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        "Access-Control-Allow-Headers": "*",
        Autorization: "Bearer " + token,
      },
    });

    debugger;
    if (response.status === 200) {
      return response;
    }
  } catch (error) {
    toast.error(error.response.data.message);
    console.error(error);
    return error;
  }
};
